# Public Health Monitoring Platform

![CallME Logo](/assets/callme-logo.png)

### Presentation
[![Presentation](https://img.youtube.com/vi/H3_HGiqQfxE/0.jpg)](https://www.youtube.com/watch?v=H3_HGiqQfxE)


## The BORG Team!


### Source code
99% of our platform if based on Google Cloud & Twilio serverless services, so thats way in first version we produce only one Cloud Funtion to redirect recieved data from Twilio to GCP Analitics Platform (i.e. BigQuery etc)

## Architecture

![CallME Architecture](/assets/arch.jpg)

If you are intrested in our work do not hasitate to contact us: ``support at dancom.eu``