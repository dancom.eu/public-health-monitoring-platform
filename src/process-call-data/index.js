const { PubSub } = require('@google-cloud/pubsub');
const { Firestore } = require('@google-cloud/firestore');

const firestore = new Firestore();
const patients = firestore.collection('patients');
const pubsub = new PubSub();
const topic = process.env.PUBSUB_DATA_PROCESSING;


/**
 * Save information about Patient Phone Calls
 * @param {Object} body - patient json data
 */
function savePatientData(body) {
    console.log('Patients phone call data saved');

    patients.doc(body.patientid).set({
        patientid: body.patientid,
        timestamp: body.timestamp
    }).then(status => {
        console.log(status);
    }, err => {
        console.error(err);
    });

    pubsub.topic(topic).publish(Buffer.from(JSON.stringify(body))).then(status => {
        console.log(status);
    }, err => {
        console.error(err);
    });
}

exports.processCallData = (req, res) => {
    req.body.timestamp = Date.now();

    savePatientData(req.body);
    res.status(200).json();
};