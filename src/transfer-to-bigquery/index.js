const { BigQuery } = require('@google-cloud/bigquery');
const dataSet = process.env.BIGQUERY_DATASET_NAME;
const tableId = process.env.BIGQUERY_TABLE_NAME;

const bigquery = new BigQuery();

/**
 * Save Processed already data into BigQuery
 * @param {Object} payload - JSON to save into BigQuery
 */
function saveData(payload) {
    console.log('Saving data to BigQuery');
    console.log('Inserting payload', payload);

    payload.timestamp = bigquery.timestamp(payload.timestamp);

    bigquery.dataset(dataSet).table(tableId).insert(payload).then(status => {
        console.log(status);
    }, err => {
        if (err.name === 'PartialFailureError') {
            console.error(JSON.stringify(err.errors));
        } else {
            console.error(err);
        }
    });
}

/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 * @param {!Object} event Event payload.
 * @param {!Object} context Metadata for the event.
 */
exports.processData = (event, context) => {
    const pubsubMessage = Buffer.from(event.data, 'base64').toString();
    const payload = JSON.parse(pubsubMessage);

    saveData(payload);
};